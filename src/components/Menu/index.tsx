import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import { Button, Theme, createStyles } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    flexGrow: 1
  },
  toolbar: {
    border: "1px solid gray",
    marginBottom: theme.spacing(0.5)
  }
}));

export type AppToolbarBarProps = {
  items: MenuItem[];
  onClick?: (id: string) => void;
};

export type MenuItem = {
  id: string;
  label: string;
};

export default function AppToolbarBar(props: AppToolbarBarProps) {
  const classes = useStyles();
  const { items, onClick } = props;

  const handleClick = (id: string) => () => {
    if (onClick) {
      onClick(id);
    }
  };

  return (
    <div className={classes.root}>
        <Toolbar className={classes.toolbar}>
          {items.map(({ id, label }) => (
            <Button key={id} onClick={handleClick(id)} >
              {label}
            </Button>
          ))}
        </Toolbar>
    </div>
  );
}
