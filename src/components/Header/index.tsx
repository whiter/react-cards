import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Menu, { MenuItem } from "../Menu";

const useStyles = makeStyles({
  root: {
    flexGrow: 1
  }
});

const firstMenu: MenuItem[] = [
  { id: "menu_1.1", label: "Menu item 1" },
  { id: "menu_1.2", label: "Menu item 2" },
  { id: "menu_1.32", label: "Menu item 3" }
];
const secondMenu: MenuItem[] = [
  { id: "menu_2.1", label: "Item 1" },
  { id: "menu_2.2", label: "Item 2" },
  { id: "menu_2.3", label: "Item 3" }
];

export default function Header() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Menu items={firstMenu} />
      <Menu items={secondMenu} />
    </div>
  );
}
