import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    flex: "1 1 auto",
    height: "100%",
    display: "flex"
  },
  message: {
      margin: "auto"
  }
});

type StatusMessageProps = {
  message: string
}

export default function StatusMessage(props: StatusMessageProps) {
  const classes = useStyles();
  const {message} = props

  return (
    <div className={classes.root}>
        <div className={classes.message}>{message}</div>
    </div>
  );
}
