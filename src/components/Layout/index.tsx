import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Header from "../Header";
import Footer from "../Footer";
import Content from "../Content";

const useStyles = makeStyles({
  root: {
    flexGrow: 1
  }
});

export default function Layout() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Header />
      <Content />
      <Footer />
    </div>
  );
}
