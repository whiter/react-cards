import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import StatusMessage from "../StatusMessage";
import { Card, Typography, Theme, createStyles } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flex: "1 1 auto",
      height: "100%",
      display: "flex",
      alignContent: "flex-start",
      justifyContent: "space-around",
      flexWrap: "wrap"
    },
    card: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
      height: 150,
      [theme.breakpoints.down("xl")]: {
        minWidth: 400,
        width: 400,
      },
      [theme.breakpoints.down("lg")]: {
        minWidth: 250,
        width: 250,
      },
      [theme.breakpoints.down("md")]: {
        minWidth: 150,
        width: 150,
      }
    }
  })
);

export default function Content() {
  const [state, setState] = React.useState({
    data: [],
    loading: true,
    error: false
  });
  React.useEffect(() => {
    fetch("https://stage-app.voxtis.io/user")
      .then(function(response) {
        return response.json();
      })
      .then(function(json) {
        setState({ data: json.result, loading: false, error: false });
      })
      .catch(() => {
        setState({ data: [], loading: false, error: true });
      });
  }, []);

  const classes = useStyles();
  const { data, error, loading } = state;

  if (error) {
    return (
      <StatusMessage message="Unexpected error occured when loading data. Please try later" />
    );
  }

  if (loading) {
    return <StatusMessage message="Loading..." />;
  }

  return (
    <div className={classes.root}>
      {data.map((item: any) => (
        <Card key={item.id} className={classes.card}>
          <Typography variant="h6" component="h2">
            {`${item.info.first_name} ${item.info.last_name}`}
          </Typography>
        </Card>
      ))}
    </div>
  );
}
