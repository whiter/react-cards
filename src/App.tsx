import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Header from "./components/Header";
import Content from "./components/Content";
import Footer from "./components/Footer";
import { CssBaseline } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    height: "100vh",
    display: "flex",
    flexDirection: "column"
  }
});

const App: React.FC = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Header />
      <Content />
      <Footer />
    </div>
  );
};

export default App;
